import { faWhatsapp } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope, faFileLines } from "@fortawesome/free-regular-svg-icons";
import {
  faArrowTrendUp,
  faBox,
  faComments,
  faGears,
  faHouse,
  faLock,
  faMapLocationDot,
  faMarsAndVenus,
  faPalette,
  faRulerVertical,
  faTags,
  faUsers,
  faWeightHanging,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Checkroom } from "@mui/icons-material";

export const listItems = [
  {
    text: "lorem ipsum",
    icon: <FontAwesomeIcon icon={faBox} />,
    props: [
      {
        text: "IT Apps / Developer",
        icon: <Checkroom />,
        path: "/",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faWeightHanging} />,
        path: "/b",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faPalette} />,
        path: "/c",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faRulerVertical} />,
        path: "/d",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faMarsAndVenus} />,
        path: "/e",
      },
    ],
  },
  {
    text: "dolot sit amet",
    icon: <FontAwesomeIcon icon={faFileLines} />,
    props: [
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faHouse} />,
        path: "/f",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faArrowTrendUp} />,
        path: "/g",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faUsers} />,
        path: "/h",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faTags} />,
        path: "/i",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faComments} />,
        path: "/j",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faEnvelope} />,
        path: "/k",
      },
    ],
  },
  {
    text: "lorem ipsum",
    icon: <FontAwesomeIcon icon={faGears} />,
    props: [
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faLock} />,
        path: "/l",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faWhatsapp} />,
        path: "/m",
      },
      {
        text: "lorem",
        icon: <FontAwesomeIcon icon={faMapLocationDot} />,
        path: "/n",
      },
    ],
  },
];

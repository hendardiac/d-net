import React, { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark } from "@fortawesome/free-solid-svg-icons";
import { Divider, IconButton, Paper, Typography } from "@mui/material";
import LinearProgress from "@mui/material/LinearProgress";
import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";
import MoreVertIcon from "@mui/icons-material/MoreVert";

import "./HomeCard.css";

const options = [
  "None",
  "Atria",
  "Callisto",
  "Dione",
  "Ganymede",
  "Hangouts Call",
  "Luna",
  "Oberon",
  "Phobos",
  "Pyxis",
  "Sedna",
  "Titania",
  "Triton",
  "Umbriel",
];

const ITEM_HEIGHT = 48;

const HomeCard = ({ props }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      setProgress((oldProgress) => {
        if (oldProgress === 100) {
          return 0;
        }
        const diff = Math.random() * 10;
        return Math.min(oldProgress + diff, 100);
      });
    }, 500);

    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <Paper
      className="home__card"
      elevation={4}
      style={{ height: "100%", padding: "2rem" }}
    >
      <div className="home__card__menu">
        <IconButton
          aria-label="more"
          id="long-button"
          aria-controls={open ? "long-menu" : undefined}
          aria-expanded={open ? "true" : undefined}
          aria-haspopup="true"
          onClick={handleClick}
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          MenuListProps={{
            "aria-labelledby": "long-button",
          }}
          anchorEl={anchorEl}
          open={open}
          onClose={handleClose}
          PaperProps={{
            style: {
              maxHeight: ITEM_HEIGHT * 4.5,
              width: "20ch",
            },
          }}
        >
          {options.map((option) => (
            <MenuItem
              key={option}
              selected={option === "Pyxis"}
              onClick={handleClose}
            >
              {option}
            </MenuItem>
          ))}
        </Menu>
      </div>
      <img src={props.image_url} alt="card" />
      <div className="home__card__progress">
        <Typography variant="body2">acheivement</Typography>
        <Typography
          variant="h5"
          style={progress > 30 ? { color: "#3949ab" } : { color: "#d32f2f" }}
          sx={{ fontWeight: "bold" }}
        >
          {parseInt(progress)}%
        </Typography>
        <LinearProgress
          color={progress > 30 ? "primary" : "error"}
          variant="determinate"
          value={progress}
          sx={{ borderRadius: "10px", height: "12px" }}
        />
      </div>
      <Divider />
      <div className="home__card__title">
        <FontAwesomeIcon
          style={progress > 30 ? { color: "#3949ab" } : { color: "#d32f2f" }}
          className="home__card__title__icon"
          icon={faBookmark}
        />
        <h3 className="home__card__title__text">{props.title}</h3>
      </div>
      <Typography style={{ paddingLeft: "2.2vw" }} variant="body2">
        bobot kpi : {parseInt(progress)}%
      </Typography>
      <Divider />
      <div className="home__card__description">
        <Typography
          variant="h3"
          style={progress > 30 ? { color: "#3949ab" } : { color: "#d32f2f" }}
        >
          {parseInt(progress)}%
        </Typography>
        <Typography variant="h5">{props.title}</Typography>
        <Typography variant="body2">{props.description}</Typography>
      </div>
    </Paper>
  );
};

export default HomeCard;

import React, { useEffect, useState } from "react";
import { Stack, Typography } from "@mui/material";
import LinearProgress from "@mui/material/LinearProgress";

const HomeAchievement = () => {
  const [progress, setProgress] = useState(0);

  useEffect(() => {
    const timer = setInterval(() => {
      setProgress((oldProgress) => {
        if (oldProgress === 100) {
          return 0;
        }
        const diff = Math.random() * 10;
        return Math.min(oldProgress + diff, 100);
      });
    }, 500);

    return () => {
      clearInterval(timer);
    };
  }, []);

  return (
    <Stack
      direction="column"
      justifyContent="space-between"
      sx={{ height: "100%" }}
      spacing={2}
    >
      <Stack direction="row" justifyContent="space-between">
        <Stack direction="column">
          <Typography variant="body2" py={1}>
            Achievement on 2019
          </Typography>
          <Typography variant="h4" sx={{ fontWeight: "bold" }}>
            {(Math.round(progress * 100) / 100).toFixed(2)}%
          </Typography>
        </Stack>
        <img
          src="assets/images/home/people_running.png"
          style={{ objectFit: "cover" }}
          alt=""
        />
      </Stack>
      <LinearProgress
        color={progress > 30 ? "primary" : "error"}
        variant="determinate"
        value={progress}
        sx={{ borderRadius: "10px", height: "12px" }}
      />
    </Stack>
  );
};

export default HomeAchievement;

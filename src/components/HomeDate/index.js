import React from "react";
import { Box, Paper, Stack, Typography } from "@mui/material";
import IconButton from "@mui/material/IconButton";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCalendar } from "@fortawesome/free-solid-svg-icons";

const HomeDate = () => {
  return (
    <Stack alignItems="flex-end" direction="row" justifyContent="space-between">
      <Box>
        <Typography variant="body2" py={2}>
          Date
        </Typography>
        <Typography variant="h6">February 2019</Typography>
        <Typography variant="body2">Achievement Date</Typography>
      </Box>
      <Box>
        <IconButton aria-label="more" id="long-button" aria-haspopup="true">
          <Paper
            elevation={2}
            sx={{
              display: "flex",
              alignItems: "center",
              padding: "0.8rem 0.9rem",
              justifyContent: "center",
            }}
          >
            <FontAwesomeIcon
              icon={faCalendar}
              style={{ color: "#3949ab", fontSize: "0.9rem" }}
            />
          </Paper>
        </IconButton>
      </Box>
    </Stack>
  );
};

export default HomeDate;

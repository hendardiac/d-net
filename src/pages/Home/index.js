import React from "react";
import { Grid } from "@mui/material";
import HomeDate from "../../components/HomeDate";
import HomeDeveloper from "../../components/HomeDeveloper";
import HomeAchievement from "../../components/HomeAchievement";
import HomeCard from "../../components/HomeCard";

const cards = [
  {
    image_url: "assets/images/home/card_1.png",
    title: "Timeline",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quae eum tenetur, aperiam nostrum voluptatibus nam odio eligendi accusantium ducimus. Maxime.",
  },
  {
    image_url: "assets/images/home/card_2.png",
    title: "Data Validation",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro, velit?",
  },
  {
    image_url: "assets/images/home/card_3.png",
    title: "Document Availability",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. A mollitia error eum officia facilis neque!",
  },
  {
    image_url: "assets/images/home/card_4.png",
    title: "Performance Apps",
    description:
      "Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quae eum tenetur, aperiam nostrum voluptatibus nam odio eligendi accusantium ducimus. Maxime.",
  },
  {
    image_url: "assets/images/home/card_5.png",
    title: "Research New Technology",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. A mollitia error eum officia facilis neque!",
  },
  {
    image_url: "assets/images/home/card_6.png",
    title: "Training",
    description:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Porro, velit?",
  },
];

const Home = () => {
  return (
    <>
      <Grid container spacing={4}>
        <Grid item xs={12} lg={3}>
          <HomeDeveloper />
        </Grid>
        <Grid item xs={12} lg={3}>
          <HomeDate />
        </Grid>
        <Grid item xs={12} lg={2}></Grid>
        <Grid item xs={12} lg={4}>
          <HomeAchievement />
        </Grid>
      </Grid>
      <Grid container spacing={4} my={4}>
        {cards.map((card, index) => {
          return (
            <Grid item xs={12} lg={3} key={index}>
              <HomeCard props={card} />
            </Grid>
          );
        })}
      </Grid>
    </>
  );
};

export default Home;
